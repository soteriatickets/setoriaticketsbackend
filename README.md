
Project name is `setoriaTicketsBackend`
## **Creating The Enviroments**
the command is `source venv/bin/activate` is only for linux machines
```
python -m virtualenv venv
source venv/bin/activate
python -m pip install django
python -m pip install -r requirments.txt
python manage.py makemigrations
python manage.py migrate
```

## **Start The Server**
```
python manage.py runserver
```

## **Creating Admin User**
```
python manage.py createsuperuser
# ...Follow the instructions
```