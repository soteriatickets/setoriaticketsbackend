from django.contrib import admin
from base.admin import READONLY_FIELDS, RECORD_INFO
from .models import *

# Register your models here.

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
	readonly_fields = READONLY_FIELDS

@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
	readonly_fields = READONLY_FIELDS

@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
	readonly_fields = READONLY_FIELDS
@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
	readonly_fields = READONLY_FIELDS