from django.db import models
from base.models.abstract import Base

TYPE_CHOICES = [
	("hall", "Hall"),
]
class Location(Base):


	name = models.CharField(verbose_name="Name", max_length=50)
	description = models.TextField(verbose_name="Description", null=True, blank=True)
	type = models.CharField("Type", max_length=50, choices=TYPE_CHOICES, default="hall")
	country_id = models.ForeignKey(
		"base.Country",
		verbose_name="Country",
		on_delete=models.RESTRICT,
		related_name="location_ids",
		related_query_name="location_ids",
		db_column="country_id",
	)
	region_id = models.ForeignKey(
		"base.Region",
		verbose_name="Region",
		on_delete=models.SET_NULL,
		null=True,
		blank=True,
		related_name="location_ids",
		related_query_name="location_ids",
		db_column="region_id",
	)
	sub_region_id = models.ForeignKey(
		"base.SubRegion",
		verbose_name="Sub-Region",
		on_delete=models.SET_NULL,
		null=True,
		blank=True,
		related_name="location_ids",
		related_query_name="location_ids",
		db_column="sub_region_id",
	)
	city_id = models.ForeignKey(
		"base.City",
		verbose_name="City",
		on_delete=models.RESTRICT,
		related_name="location_ids",
		related_query_name="location_ids",
		db_column="city_id",
	)
	address = models.TextField(verbose_name="Address")
	lon = models.FloatField(verbose_name="Longitude", max_length=10, )
	lat = models.FloatField(verbose_name="Latitude", max_length=10, )
	user_id = models.ForeignKey(
		"base.User",
		verbose_name="Owner",
		on_delete=models.RESTRICT,
		null=True,
		blank=True,
		related_name="location_ids",
		related_query_name="location_ids",
		db_column="user_id",
		limit_choices_to={'is_event_coordinator': True}
	)
	hall_map = models.ImageField(verbose_name="Hall Map", null=True, blank=True)
