from django.db import models
from django.utils import timezone
from base.models.abstract import Base
from .ticket import Ticket
from computedfields.models import computed

class Event(Base):
	STATE_SELELCTION = [
		('draft', 'Draft'),
		('selling', 'Selling'),
		('end_selling', 'End Selling'),
		('expired', 'Expired'),
		('cancelled', 'Cancelled')
	]

	name = models.CharField("Name", max_length=70)
	state = models.CharField("Status", choices=STATE_SELELCTION, default='draft', max_length=20, editable=False)
	description = models.TextField("Description", null=True, blank=True)
	image = models.ImageField("Image", null=True, blank=True)
	url = models.URLField("Link", null=True, blank=True)
	location_id = models.ForeignKey(
        "Location",
		on_delete=models.RESTRICT,
        verbose_name="Location",
        related_name="event_ids",
        related_query_name="event_ids",
        db_column="location_id",
    )
	event_date = models.DateTimeField("Event Date")
	start_date = models.DateTimeField("Start Selling Date", default=timezone.now())
	end_date = models.DateTimeField("End Selling Date", blank=True)

	def save(self, *args, **kwargs):
		if self.event_date and not self.end_date:
			self.end_date = self.event_date
		response = super(Event, self).save(*args, **kwargs)
		return response

	@computed(models.BooleanField("Is Sold Out"), depends=[['ticket_ids', ['state']]])
	def is_sold_out(self):
		tickets_created = Ticket.objects.filter(event_id=self.id)
		if tickets_created:
			unsold_tickets = Ticket.objects.filter(event_id=self.id, state__in=['draft', 'init_sale'])
			if unsold_tickets:
				return False
			else:
				return True
		else:
			return False

	def set_to_selling(self):
		self.state = 'selling'
		self.save()

	def set_to_end_selling(self):
		self.state = 'end_selling'
		self.save()

	def set_to_expired(self):
		self.state = 'expired'
		self.save()

	def set_to_cancelled(self):
		self.state = 'cancelled'
		self.save()