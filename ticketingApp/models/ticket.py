from django.db import models
from django.forms import ValidationError
from base.models.abstract import Base
from computedfields.models import computed

class Ticket(Base):
	STATE_SELECTION = [
		('draft', 'Draft'),
		('init_sale', 'Initial Sale'),
		('second_sale', 'Reselling'),
		('sold', 'Sold'),
		('cancel', 'Cancelled')
	]

	name = models.CharField("Number", unique=True, max_length=40)
	state = models.CharField("Status", choices=STATE_SELECTION, default='draft', editable=False, max_length=20)
	event_id = models.ForeignKey(
        "Event",
        verbose_name="Event",
		on_delete=models.CASCADE,
        related_name="ticket_ids",
        related_query_name="ticket_ids",
        db_column="event_id",
    )
	user_id = models.ForeignKey(
        "base.User",
        verbose_name="Owner",
		on_delete=models.RESTRICT,
		null=True,
		blank=True,
        related_name="ticket_ids",
        related_query_name="ticket_ids",
        db_column="user_id",
    )
	init_price = models.FloatField("Initial Price")
	current_price = models.FloatField("Current Price")

	@computed(models.DateTimeField("Selling Date"), depends=[["event_id", ["start_date"]]])
	def selling_date(self):
		return self.event_id.start_date

	@computed(models.DateTimeField("Expire Date"), depends=[["event_id", ["start_date"]]])
	def expire_date(self):
		return self.event_id.end_date

	def save(self, *args, **kwargs):
		duplicate_number_found = self.event_id.ticket_ids.filter(name=self.name).exclude(id=self.id)
		if duplicate_number_found:
			raise ValidationError("Duplicate ticket number found")
		res = super(Ticket, self).save(*args, **kwargs)
		return res

	def set_state_to_init_sale(self):
		self.state = 'init_sale'
		self.save()
	
	def set_state_to_second_sale(self):
		self.state = 'second_sale'
		self.save()
	
	def set_state_to_sold(self):
		self.state = 'sold'
		self.save()
	
	def set_state_to_cancel(self):
		self.state = 'cancel'
		self.save()

	def __str__(self):
		return self.name