from datetime import datetime
from django.db import models
# from django.utils import timezone
from base.models.abstract import Base

class Transaction(Base):
	name = models.CharField("Transaction Number", max_length=256)
	from_address = models.CharField("From Address", max_length=256)
	to_address = models.CharField("To Address", max_length=256)
	ticket_id = models.ForeignKey(
        "Ticket",
        verbose_name="Ticket",
		on_delete=models.RESTRICT,
        related_name="transaction_ids",
        related_query_name="transaction_ids",
        db_column="ticket_id",
    )
	amount = models.FloatField("Amount")
	seller_id = models.ForeignKey(
        "base.User",
        verbose_name="Seller",
		on_delete=models.RESTRICT,
        related_name="transaction_seller_ids",
        related_query_name="transaction_seller_ids",
        db_column="seller_id",
    )
	buyer_id = models.ForeignKey(
        "base.User",
        verbose_name="Buyer",
		on_delete=models.RESTRICT,
        related_name="transaction_buyer_ids",
        related_query_name="transaction_buyer_ids",
        db_column="buyer_id",
    )
	date = models.DateTimeField("Timestamp", default=datetime.utcnow() )

	def __str__(self) -> str:
		return self.name