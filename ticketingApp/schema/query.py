import graphene
from ticketingApp import models as ticketingAppModels
from . import objects

class Query(graphene.ObjectType):
	all_locations = graphene.List(objects.LocationObjectType)
	all_events = graphene.List(objects.EventType)
	all_tickets = graphene.List(objects.TicketType)
	all_transactions = graphene.List(objects.TransactionType)
	
	def resolve_all_locations(root, info, **kwargs):
		return ticketingAppModels.Location.objects.all()

	def resolve_all_events(root, info, **kwargs):
		return ticketingAppModels.Event.objects.all()

	def resolve_all_tickets(root, info, **kwargs):
		return ticketingAppModels.Ticket.objects.all()

	def resolve_all_transactions(root, info, **kwargs):
		return ticketingAppModels.Transaction.objects.all()