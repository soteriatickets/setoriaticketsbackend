from ticketingApp import models as ticketingAppModels
from graphene_django import DjangoObjectType

class TicketType(DjangoObjectType):
	class Meta:
		model = ticketingAppModels.Ticket
		fields = (
			'id',
			'name',
			'state',
			'event_id',
			'user_id',
			'init_price',
			'current_price',
		)
