from ticketingApp import models as ticketingAppModels
from graphene_django import DjangoObjectType

class EventType(DjangoObjectType):
	class Meta:
		model = ticketingAppModels.Event
		fields = (
			'id',
			'name',
			'state',
			'description',
			'image',
			'url',
			'location_id',
			'event_date',
			'start_date',
			'end_date'
		)
