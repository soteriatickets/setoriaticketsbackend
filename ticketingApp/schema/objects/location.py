from ticketingApp import models as ticketingAppModels
from graphene_django import DjangoObjectType

# Changed the name from LocationType to LocationObjectType
# because the first one kept raising errors
class LocationObjectType(DjangoObjectType):
	class Meta:
		model = ticketingAppModels.Location
		fields = (
			'id',
			'name',
			'description',
			'type',
			'country_id',
			'region_id',
			'sub_region_id',
			'city_id',
			'address',
			'lon',
			'lat',
			'hall_map'
		)