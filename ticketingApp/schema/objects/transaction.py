from ticketingApp import models as ticketingAppModels
from graphene_django import DjangoObjectType

class TransactionType(DjangoObjectType):
	class Meta:
		model = ticketingAppModels.Transaction
		fields = (
			'id',
			'name',
			'from_address',
			'to_address',
			'ticket_id',
			'amount',
			'seller_id',
			'buyer_id',
			'date',
		)
