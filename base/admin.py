from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *

# Register your models here.
READONLY_FIELDS = ['id', 'create_uid', 'last_update_uid', 'create_date', 'last_update_date']

RECORD_INFO = ('Record Info', {
            'fields': ('create_uid', 'create_date', 'last_update_uid', 'last_update_date')
        })

@admin.register(User)
class CustomUserAdmin(UserAdmin):

	search_fields = ('email','username')
	ordering = ('email','username')

	readonly_fields = READONLY_FIELDS + ['last_login']

	fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal info', {
            'fields': ('first_name', 'last_name', 'email', 'country_id', 'region_id', 'sub_region_id', 'city_id')
        }),
        ('Profile Info', {
            'fields': ('image', 'gender', 'phone', 'is_event_coordinator')
        }),
        ('Important dates', {
            'fields': ('last_login', 'date_joined')
        }),
        ('Permissions', {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        RECORD_INFO,
    )

@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
	readonly_fields = READONLY_FIELDS

@admin.register(DeviceBrand)
class DeviceBrandAdmin(admin.ModelAdmin):
	readonly_fields = READONLY_FIELDS