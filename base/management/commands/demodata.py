import glob, datetime
import subprocess
from pathlib import Path

import faker.providers
from base.models import Device, DeviceBrand
# from base.models import Location
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db import connection
from faker import Faker

PASSWORD_FIELD = "password"

ADMIN_USERS = [
    {
        "username": "admin",
        "email": "test@email.com",
        "password": "admin",
        "gender": "male",
        "job_title": "Developer",
    },
]

BRANDS = [
    "Samsung",
    "Apple",
    "Google",
    "Huawei",
    "OnePlus",
    "Xiaomi",
    "LG",
    "Oppo",
    "Vivo",
    "Nokia",
]

class Provider(faker.providers.BaseProvider):
    def device_brand(self):
        return self.random_element(BRANDS)
    
class Command(BaseCommand):
    help = "Create data for testing"
    requires_migrations_checks = False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        Faker.seed(0)
        self.fake = Faker()
        self.fake.add_provider(Provider)
        self.UserModel = get_user_model()

        self.num_of_users = 10
        self.num_device_brand = 10
        self.num_device = 10


    def handle(self, *args, **options):
        self.stdout.write(
            "Demo Databasse will be created, Warning: The database and the migrations Will be deleted!"
        )
        answer = input(
            "Are you sure you want to delete the database and the migrations? (y/N)"
        )

        if not answer.lower().strip() == "y":
            self.stdout.write(self.style.WARNING(f"Command aborted!"))
        else:
            # Delete the Database
            db_settings = connection.settings_dict
            if db_settings["ENGINE"] == "django.db.backends.sqlite3":
                db_path = glob.glob("db.sqlite3", recursive=True)
                if db_path:
                    self.stdout.write("Database Type: sqlite3")
                    Path(db_path[0]).unlink()
                    self.stdout.write(
                        self.style.SUCCESS("Database Deleted Successfully!")
                    )
                else:
                    pass
            elif db_settings["ENGINE"] == "django.db.backends.postgresql_psycopg2":
                pass

            # Delete the Migrations
            for file_name in glob.glob("*/migrations/[!__init__.py]*", recursive=True):
                Path(file_name).unlink()

            # Create the Migrations and Migrate
            subprocess.run(["python", "manage.py", "makemigrations"])
            subprocess.run(["python", "manage.py", "migrate"])

            # Start Creating Demo Database
            self.stdout.write("Start Creating Demo Database")
			# TODO: uncomment when you need it
            # self.create_location()
            self.create_admin_user()
            self.create_normal_users(self.num_of_users)
            self.create_device_brand()
            self.create_device(self.num_device)

            self.stdout.write(self.style.SUCCESS("Demo Database Created Successfully!"))

    # def create_location(self, num_working_location=None):
    #     self.stdout.write("Creating working locations")
    #     for _ in range(
    #         num_working_location if num_working_location else self.num_of_location
    #     ):
    #         location = self.fake.location_on_land()
    #         Location.objects.create(
    #             name=f"{location[2]} - {location[4]}",
    #             latitude=self.fake.latitude(),
    #             longitude=self.fake.longitude(),
    #             radius=100.55344,
    #         )
    #     self.stdout.write(
    #         self.style.SUCCESS(f"--- Working Locations Created Successfully! ---")
    #     )
    #     self.stdout.write(
    #         self.style.SUCCESS(
    #             f"Number of Working Locations: {Location.objects.all().count()}"
    #         )
    #     )

    def create_admin_user(self):
        self.stdout.write("Creating Admin Users and Employees")
        for admin_user in ADMIN_USERS:
            width = 180
            height = 180
            image = self.fake.image_url(
                width=width,
                height=height,
                placeholder_url=f"https://via.placeholder.com/{width}x{height}",
            )
            birth_date = self.fake.date_of_birth(minimum_age=18, maximum_age=30)
            phone = self.fake.unique.phone_number()
            user = self.UserModel.objects.create_superuser(
                admin_user["username"],
				admin_user["email"],
				admin_user["password"],
				image = image,
				birth_date = birth_date,
				phone = phone,
            )
            self.stdout.write(
                self.style.SUCCESS(
                    f"- Admin User and Employee created successfully for {user.username}!"
                )
            )
        self.stdout.write(
            self.style.SUCCESS(
                f"--- Admin Users and Employees Created Successfully! ---"
            )
        )

    def create_device_brand(self):
        self.stdout.write("Creating Device brand records")
        for _ in range(self.num_device_brand):
            DeviceBrand.objects.create(
                name=self.fake.unique.device_brand(),
            )

        self.stdout.write(
            self.style.SUCCESS(f"--- Device brands Created Successfully! ---")
        )

    def create_device(self, num_device=None):
        self.stdout.write("Creating Device records")
        for i in range(num_device if num_device else self.num_device):
            user = self.UserModel.objects.order_by("?").first()
            device_brand = DeviceBrand.objects.order_by("?").first()
            Device.objects.create(
                name="Phone",
                user_id=user,
                brand_id=device_brand,
            )
        self.stdout.write(self.style.SUCCESS(f"--- Device Created Successfully! ---"))


    def create_normal_users(self, num_of_users=None):
        self.stdout.write("Creating Users")
        for i in range(num_of_users if num_of_users else self.num_user_and_emp):
            is_male = self.fake.pybool()
            username = self.fake.unique.user_name()
            first_name = (
                self.fake.first_name_male()
                if is_male
                else self.fake.first_name_female()
            )
            last_name = self.fake.last_name()
            email = self.fake.email()
            width = 180
            height = 180
            image = self.fake.image_url(
                width=width,
                height=height,
                placeholder_url=f"https://via.placeholder.com/{width}x{height}",
            )
            birth_date = self.fake.date_of_birth(minimum_age=18, maximum_age=80)
            phone = self.fake.unique.phone_number()

            user = self.UserModel.objects.create_user(
                username=username,
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=username,
                image=image,
                birth_date=birth_date,
                phone=phone,
            )
            self.stdout.write(
                self.style.SUCCESS(
                    f"{i} - User ({user.get_full_name()}) created successfully!"
                )
            )
        self.stdout.write(
            self.style.SUCCESS(f"--- Users and Employees Created Successfully! ---")
        )

    def create_device_brand(self):
        self.stdout.write("Creating Device brand records")
        for _ in range(self.num_device_brand):
            DeviceBrand.objects.create(
                name=self.fake.unique.device_brand(),
            )

        self.stdout.write(
            self.style.SUCCESS(f"--- Device brands Created Successfully! ---")
        )

    def create_device(self, num_device=None):
        self.stdout.write("Creating Device records")
        for i in range(num_device if num_device else self.num_device):
            user = self.UserModel.objects.order_by("?").first()
            device_brand = DeviceBrand.objects.order_by("?").first()
            Device.objects.create(
                name="Test",
                user_id=user,
                brand_id=device_brand,
            )
        self.stdout.write(self.style.SUCCESS(f"--- Device Created Successfully! ---"))