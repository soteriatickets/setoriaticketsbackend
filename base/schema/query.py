import graphene
from base import models as BaseModels
from . import objects
from graphql_auth.schema import UserQuery, MeQuery

class Query(UserQuery, MeQuery, graphene.ObjectType):
	all_countries = graphene.List(objects.CountryType)
	all_regions = graphene.List(objects.RegionType)
	all_subregions = graphene.List(objects.SubRegionType)
	all_cities = graphene.List(objects.CityType)
	
	def resolve_all_countries(root, info, **kwargs):
		return BaseModels.Country.objects.all()

	def resolve_all_regions(root, info, **kwargs):
		return BaseModels.Region.objects.all()

	def resolve_all_subregions(root, info, **kwargs):
		return BaseModels.SubRegion.objects.all()

	def resolve_all_cities(root, info, **kwargs):
		return BaseModels.City.objects.all()