from base import models as BaseModels
from graphene_django import DjangoObjectType

class CountryType(DjangoObjectType):
	class Meta:
		model = BaseModels.Country
		fields = (
			'id',
			'name',
			'code2',
			'code3',
			'phone',
		)

class RegionType(DjangoObjectType):
	class Meta:
		model = BaseModels.Region
		fields = (
			'id',
			'name',
			'geoname_code',
			'country'
		)

class SubRegionType(DjangoObjectType):
	class Meta:
		model = BaseModels.SubRegion
		fields = (
			'id',
			'name',
			'geoname_code',
			'country',
			'region',
		)

class CityType(DjangoObjectType):
	class Meta:
		model = BaseModels.City
		fields = (
			'id',
			'name',
			'latitude',
			'longitude',
			'feature_code',
			'timezone',
			'country',
			'region',
			'subregion',
		)