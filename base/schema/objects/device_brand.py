from base import models as BaseModels
from graphene_django import DjangoObjectType

class DeviceBrandType(DjangoObjectType):
	class Meta:
		model = BaseModels.DeviceBrand
		fields = (
			'id',
			'name',
		)