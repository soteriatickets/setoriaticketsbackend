from base import models as BaseModels
from graphene_django import DjangoObjectType

class UserType(DjangoObjectType):
	class Meta:
		model = BaseModels.User
		fields = (
			'id',
			'username',
			'gender',
			'phone',
			'country_id',
			'region_id',
			'sub_region_id',
			'city_id',
			'last_login',
			'birth_date',
			'device_ids'
		)