from base import models as BaseModels
from graphene_django import DjangoObjectType

class DeviceType(DjangoObjectType):
	class Meta:
		model = BaseModels.Device
		fields = (
			'name',
		)