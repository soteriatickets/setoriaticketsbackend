from .abstract import Base
from django.db import models
from cities_light.abstract_models import AbstractCountry, AbstractCity, AbstractRegion, AbstractSubRegion

class Country(AbstractCountry, Base):
	"Creating New Country Model"
	slug = models.CharField("Slug", max_length=50)

class Region(AbstractRegion, Base):
	"Creating New Region Model"
	country = models.ForeignKey(Country, on_delete=models.CASCADE)

class SubRegion(AbstractSubRegion, Base):
	"Creating New Region Model"
	country = models.ForeignKey(Country, on_delete=models.CASCADE)
	region = models.ForeignKey(Region, on_delete=models.CASCADE)

class City(AbstractCity, Base):
	"Creating New City Model"
	region = models.ForeignKey(Region, on_delete=models.CASCADE)
	subregion = models.ForeignKey(SubRegion, on_delete=models.CASCADE)
	country = models.ForeignKey(Country, on_delete=models.CASCADE)