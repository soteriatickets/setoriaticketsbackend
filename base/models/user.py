from django.db import models
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField
from .abstract import Base

def user_image_directory_path(instance, filename):
	return f"users/user_{instance.pk}/images/{filename}"

GENDER_SELECTION = [("male", "Male"), ("female", "Female"), ("other", "Other")]

class User(AbstractUser, Base):
	# Personal Info
	USERNAME_FIELD = "username"   # e.g: "username", "email"
	EMAIL_FIELD = "email"         # e.g: "email", "primary_email"

	image = models.ImageField(
		"Image",
		default="default_image.png",
		upload_to=user_image_directory_path,
		null=True,
		blank=True,
	)
	gender = models.CharField("Gender", choices=GENDER_SELECTION, max_length=10)
	phone = PhoneNumberField("Phone", unique=True, null=True, blank=True)
	country_id = models.ForeignKey(
		"Country",
		verbose_name="Country",
		on_delete=models.SET_NULL,
		null=True,
		blank=True,
		related_name="user_ids",
		related_query_name="user_ids",
		db_column="country_id",
	)
	region_id = models.ForeignKey(
		"Region",
		verbose_name="Region",
		on_delete=models.SET_NULL,
		null=True,
		blank=True,
		related_name="user_ids",
		related_query_name="user_ids",
		db_column="region_id",
	)
	sub_region_id = models.ForeignKey(
		"SubRegion",
		verbose_name="Sub-Region",
		on_delete=models.SET_NULL,
		null=True,
		blank=True,
		related_name="user_ids",
		related_query_name="user_ids",
		db_column="sub_region_id",
	)
	city_id = models.ForeignKey(
		"City",
		verbose_name="City",
		on_delete=models.SET_NULL,
		null=True,
		blank=True,
		related_name="user_ids",
		related_query_name="user_ids",
		db_column="city_id",
	)
	last_login = models.DateTimeField(
		"Last Login",
		null=True,
		blank=True,
		editable=False,
	)
	birth_date = models.DateField("Birth Date", null=True, blank=True)
	is_event_coordinator = models.BooleanField(verbose_name="Is Event Coordinator", default=False)