import uuid
from model_utils.fields import AutoCreatedField,  AutoLastModifiedField

from crum import get_current_user
from django.conf import settings
from django.db import models
from django.db.models.query import QuerySet
from django.db.models.manager import BaseManager as DjangoBaseManager

from computedfields.models import ComputedFieldsModel, computed


class BaseQuerySet(QuerySet):

	def update(self, **kwargs):
		rows = super(BaseQuerySet, self).update(**kwargs)
		self.run_save_function()
		return rows

	def run_save_function(self) -> None:
		""" Run save() on all self records """
		# this is useful for computed field and function that needs save() to run
		for record in self:
			record.save()


class BaseManager(DjangoBaseManager.from_queryset(BaseQuerySet)):
	pass

class Base(ComputedFieldsModel, models.Model):

	objects = BaseManager()
	create_date = AutoCreatedField("Created at", editable=False)
	last_update_date = AutoLastModifiedField("Updated at", editable=False)
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	# since on_delete is set to null then you have to set null=true
	create_uid = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		db_column="create_uid",
		related_name="+",
		related_query_name="+",
		on_delete=models.SET_NULL,
		null=True,
		blank=True,
		editable=False,
	)

	create_uid = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		verbose_name="Create By",
		db_column="create_uid",
		related_name="+",
		related_query_name="+",
		on_delete=models.SET_NULL,
		null=True,
		blank=True,
		editable=False
		)

	last_update_uid = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		verbose_name="Last Updated By",
		db_column="last_update_uid",
		related_name="+",
		related_query_name="+",
		on_delete=models.SET_NULL,
		null=True,
		blank=True,
		editable=False
		)

	class Meta:
		abstract = True
		ordering = ("-create_date", "-last_update_date")

	def save(self, *args, **kwargs):
		# TODO: on the admin form, this returns NONE
		# Make sure it return the user object in graphql
		user = get_current_user()

		update_fields = kwargs.get("update_fields", None)
		if update_fields:
			kwargs["update_fields"] = set(update_fields).union({"last_update_date"})
		if user:
			if not self.pk:
				self.create_uid = user
			self.last_update_uid = user
		return super(Base, self).save(*args, **kwargs)

	
	def __str__(self) -> str:
		try:
			return self.name
		except AttributeError:
			return super().__str__()