from django.db import models

from .abstract import Base


class DeviceBrand(Base):
    name = models.CharField("Brand Name", max_length=150)

    def __str__(self):
        return f"{self.name} Brand"