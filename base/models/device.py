from django.conf import settings
from django.db import models
from computedfields.models import ComputedFieldsModel
from .abstract import Base

class Device(Base, ComputedFieldsModel):

	name = models.CharField("Device Name", max_length=150)
	user_id = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		verbose_name="User",
		on_delete=models.SET_NULL,
		null=True,
		blank=True,
		related_name="device_ids",
		related_query_name="device_ids",
		db_column="user_id",
	)
	brand_id = models.ForeignKey(
		"DeviceBrand",
		verbose_name="Device Brand",
		on_delete=models.RESTRICT,
		related_name="device_ids",
		related_query_name="device_ids",
		db_column="brand_id",
	)
	registration_id = models.TextField(verbose_name="Registration token", blank=True, null=True)

	def __str__(self):
		return f"{self.user_id} Device [{self.name}]"